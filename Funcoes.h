#include <iostream>
#include <windows.h>
#include <string.h>
#include <fstream>
#include <locale.h>
#include <conio2.h>
#include <iomanip>
#include <stdio.h>
#include "graphicLibrary.h"

using namespace std;
typedef double* pontmedia;
const char* ArquivoAlunos = "Alunos.txt";
const char* ArquivoProfessores = "Professores.txt";
char* arquivofun = "Funcionarios.txt";
int qtdaluno = 500;
int qtdprof = 20;
void InsereFaltas();
void MenuProfessor();
void ListarProf();
void MenuCoordenacao();
void DadosAlunos();
void DadosFun();
void DadosProf();
void IniciarArquivoAlunos();
void IniciarArquivoProf();
void CadastraAlunos();
void CadastraProf();
void ListarAlunos();
void RemoverAluno();
void AtualizarAluno();
void FuncaoAlunoMenu();
void AlterarNotasAluno();
void ListarAlunoUnico();
void RemoverProfessor();
void MenuInicial(int verificadordelogin);

struct StructAlunos
{
    int Matricula;
    char Nome[50];
    char NomeMae[50];
    char NomePai[50];
    char Sexo[15];
    char Telefone[15];
    char Endereco[50];
    float Nota1;
    float Nota2;
    float Nota3;
    float Nota4;
    int faltas;
};

struct StructProfessor
{
    int id;
    char nome[50];
    char sexo[25];
    char endereco[50];
    char telefone[15];
    double salario;
};

namespace FunSpace
{

struct StructFuns
{
    int id;
    char nome[50];
    char cargo[50];
    char sexo[15];
    char telefone[15];
    char local[50];
    double salario;
};

void CriarArquivoFun(char *arquivofun);
void CadastrarFun(char *arquivofun);
void ListarFuncionarios(char *arquivofun);
void AlterarDadosFun(char *arquivofun);
void RemoveFuncionario(char *arquivofun);
void FuncaoMenuFun(char *arquivofun);
void MenuFunMenu(char *arquivofun);
int  VerificaArquivoFun(int id);
const int MAXFUNCIONARIOS = 101;

void CriarArquivoFun(char *arquivofun)
{
    system("CLS");


    system("color 9f"); /* Muda a cor do fundo */
    textcolor(LIGHTBLUE);
    textbackground(WHITE);

    gotoxy(15, 5);
    cout <<"                                                                                            ";
    gotoxy(15, 6);
    cout <<"                            CRIAR ARQUIVO INICIAL DO FUNCIONARIO                            ";
    gotoxy(15, 7);
    cout <<"                                                                                            ";
    char opcao;
    textcolor(WHITE);
    textbackground(RED);
    gotoxy(30, 9);
    cout << "Isso ira apagar sua base de dados! Deseja continuar [s/n] ? ";
    cin >> opcao;
    textcolor(WHITE);
    textbackground(LIGHTBLUE);

    if ((opcao == 's') || (opcao == 'S'))
    {
        ofstream entrada(arquivofun, ios::out | ios::trunc);
        if (entrada.fail())
        {
            gotoxy(15, 12);
            cout << "N�o foi poss�vel acessar o arquivo!" << endl;
            exit(1);
        }

        StructFuns fichaVazia = {0, "", " ", " ", " ", " ", 0.0};

        for (int i = 0; i < MAXFUNCIONARIOS; i++)
        {

            entrada.write((const char *)(&fichaVazia), sizeof(StructFuns));
        }
        namespaceGraphic::createFile();
        textcolor(WHITE);
        textbackground(GREEN);
        gotoxy(15,13);
        cout << "Arquivo criado com sucesso!" << endl;
        textcolor(WHITE);
        textbackground(LIGHTBLUE);
        gotoxy(15, 15);
        cout << "Enter para continuar";
        entrada.close();
    }
    else
    {
        gotoxy(15, 15);
        cout << "Operacao cancelada!" << endl;
    }
    gotoxy(36,15);
    getch();
    system("cls");

    DadosFun();

}
void CadastrarFun(char *arquivofun)
{

    StructFuns fun;

    system("CLS");
    textcolor(LIGHTBLUE);
    textbackground(WHITE);
    gotoxy(15, 5);
    cout <<"                                                                                            ";
    gotoxy(15, 6);
    cout <<"                                   CADASTRO DE FUNCIONARIO                                  ";
    gotoxy(15, 7);
    cout <<"                                                                                            ";

    int id;

    do
    {
        gotoxy(15, 9);
        textcolor(WHITE);
        textbackground(LIGHTBLUE);
        cout << "Entre com o ID do Funcionario entre 1 e 100: ";
        cin >> id;
    }
    while(id <= 0 || id > 100);

    fstream entrada(arquivofun,ios::in | ios::out | ios::ate);

    entrada.seekg((id-1) * sizeof(StructFuns));

    entrada.read((char*)(&fun),sizeof(StructFuns));

    if(id  != fun.id)
    {
        fun.id = id;
        gotoxy(15, 11);
        cout<<"Informe o nome do funcionario: ";
        fflush(stdin);
        cin.getline(fun.nome,50);
        gotoxy(15, 13);
        cout<<"Informe o cargo do funcionario na escola: ";
        fflush(stdin);
        cin.getline(fun.cargo,50);
        gotoxy(15, 15);
        cout<<"Informe o sexo do funcionario: ";
        fflush(stdin);
        cin.getline(fun.sexo,15);
        gotoxy(15, 17);
        cout<<"Informe o telefone para contato do funcionario: ";
        fflush(stdin);
        cin.getline(fun.telefone,15);
        gotoxy(15, 19);
        cout<<"Informe o endereco do funcionario: ";
        fflush(stdin);
        cin.getline(fun.local,50);
        gotoxy(15, 21);
        do
        {
            cout<<"Informe o salario que o funcionario ira receber: ";
            cin>>fun.salario;
        }
        while(fun.salario <=0);

        entrada.seekp((fun.id - 1) * sizeof(StructFuns));
        entrada.write((const char *)(&fun), sizeof(StructFuns));
        namespaceGraphic::insertFuncionario();
        gotoxy(15, 25);
        textcolor(WHITE);
        textbackground(GREEN);
        cout << "Funcionario inserido com sucesso!";
    }
    else
    {
        gotoxy(15, 9);
        textcolor(WHITE);
        textbackground(LIGHTBLUE);
        cout << " Funcionario cadastrado ";
    }
    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    gotoxy(15, 27);
    cout <<"Qualquer tecla para continuar";
    getch();
    system("cls");
    entrada.close();
}

void AlterarDadosFun(char *arquivofun)
{
    StructFuns fun;
    fstream saida;
    saida.open(arquivofun, ios::out | ios::in | ios::ate);

    if (saida.fail())
    {
        cout << "N�o foi poss�vel acessar o arquivo!" << endl;
        exit(1);
    }
    system("CLS");
    FunSpace::ListarFuncionarios(arquivofun);
    cout << endl;
    cout << "Menu de altera��o de dados do funcion�rio " << endl;
    cout << "                                                               ";
    cout << endl;

    int id;
    cout << "Digite o ID do funcion�rio a ser atualizado: ";
    cin >> id;

    saida.seekg((id - 1) * sizeof(StructFuns));
    saida.read((char *)(&fun), sizeof(StructFuns));
    saida.close();
    if ((fun.id > 0) && (strcmp(fun.nome, "") != 0) && (fun.id <= MAXFUNCIONARIOS))
    {
        fstream saida;
        saida.open(arquivofun, ios::out | ios::in | ios::ate);

        do
        {
            cout <<"Entre com o novo salario do funcionario"<<endl;
            cin >> fun.salario;
        }
        while(fun.salario<0);

        saida.seekp((fun.id - 1) * sizeof(StructFuns));
        saida.write((const char *)(&fun), sizeof(StructFuns));
        cout << "\nDados atualizados com sucesso!" << endl;
        namespaceGraphic::updateData();
        cout <<"Pressione qualquer tecla para continuar"<<endl;
        getch();
        saida.close();
    }
    else
    {
        cout << "Funcionario nao encontrado!" << endl;
        getch();
        saida.close();
    }

}

void RemoveFuncionario(char *arquivofun)
{
    fstream entrada(arquivofun,ios::in | ios::out | ios::ate);

    if (entrada.fail())
    {
        cout << "N�o foi poss�vel acessar o arquivo!" << endl;
        exit(1);
    }
    StructFuns fun;

    system("CLS");
    cout << endl;

    cout << "Remover Funcion�rio " << endl;

    int id;
    cout << "Digite o ID do Funcion�rio a ser removido:";
    cin >> id;

    entrada.seekg((id - 1) * sizeof(StructFuns));
    entrada.read((char *)(&fun), sizeof(StructFuns));

    if (fun.id != 0)
    {
        cout << setiosflags(ios::left)
             << setw(5) << "ID"
             << setw(15) << "Funcion�rio"
             << setw(15) << "Cargo"
             << setw(8) << "Sexo"
             << setw(15) << "Telefone"
             << setw(10) << "Local"
             << resetiosflags(ios::left)
             << setw(10) << "Sal�rio" << endl;

        cout << setiosflags(ios::left)
             << setw(5) << fun.id
             << setw(15) << fun.nome
             << setw(15) << fun.cargo
             << setw(8) << fun.sexo
             << setw(15) << fun.telefone
             << setw(10) << fun.local
             << setw(10) << setprecision(2) << resetiosflags(ios::left)
             << setiosflags(ios::fixed | ios::showpoint) << fun.salario << '\n';

        char opcao;
        cout << "\nDeseja mesmo remover este funcion�rio [s/n] ? ";
        cin >> opcao;
        if ((opcao == 's') || (opcao == 'S'))
        {
            StructFuns fichaVazia = {0, " ", " ", " ", " ", " ", 0.0};
            entrada.seekp((id - 1) * sizeof(StructFuns));
            entrada.write((const char *)(&fichaVazia), sizeof(StructFuns));
            entrada.close();
            namespaceGraphic::removeFuncionario();
            cout << "\nFuncionario removido com sucesso!" << endl;
            getch();
        }
        else
        {
            cout << "\nOperacao Cancelada!" << endl;
        }
    }
    else
    {
        cout << "\nFuncionario nao encontrado!" << endl;
        getch();
    }
}

void ListarFuncionarios(char *arquivofun)
{
    StructFuns fun;
    int k=0;
    ifstream entrada(arquivofun);

    if (entrada.fail())
    {
        cout << "Nao foi possivel acessar o arquivo!" << endl;
        exit(1);
    }
    system("CLS");

    textcolor(LIGHTBLUE);
    textbackground(WHITE);
    gotoxy(15, 5);
    cout <<"                                                                                            ";
    gotoxy(15, 6);
    cout <<"                                    LISTA DE FUNCIONARIOS                                   ";
    gotoxy(15, 7);
    cout <<"                                                                                            ";
    textcolor(WHITE);
    textbackground(WHITE+29);
    gotoxy(15, 8);
    cout << setiosflags(ios::left)
         << setw(5) << "ID"
         << setw(15) << "Funcionario"
         << setw(15) << "Cargo"
         << setw(8) << "Sexo"
         << setw(15) << "Telefone"
         << setw(10) << "Endereco"
         << resetiosflags(ios::left)
         << setw(10) << "Salario" << endl;

    entrada.read((char *)(&fun), sizeof(StructFuns));
    while(!entrada.eof())
    {
        if (fun.id != 0)
        {
            gotoxy(15,9+k);
            textcolor(WHITE);
            textbackground(WHITE + 18);
            cout << setiosflags(ios::left)
                 << setw(5) << fun.id
                 << setw(15) << fun.nome
                 << setw(15) << fun.cargo
                 << setw(8) << fun.sexo
                 << setw(15) << fun.telefone
                 << setw(10) << fun.local
                 << setw(10) << setprecision(2) << resetiosflags(ios::left)
                 << setiosflags(ios::fixed | ios::showpoint) << fun.salario << '\n';
            k++;
        }
        entrada.read((char *)(&fun), sizeof(StructFuns));
    }


    gotoxy(15, 14);
    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    system("pause");
    system("cls");
    entrada.close();

}

void MenuFunMenu(char* arquivofun)
{
    fstream arquivo(arquivofun,ios::out|ios::in|ios::ate);
    StructFuns leitura;
    int op,aux;
    if(arquivo.fail())
    {
        cout<<"Nao foi possivel acessar o arquivo!"<<endl;
        exit(1);
    }
    cout<<"  Bem vindo ao menu do funcionario"<<endl;
    do
    {
        cout<<"Por favor, insira seu id para prosseguir"<<endl;
        cin>>leitura.id;

    }
    while(aux!=0);

    system("cls");
    arquivo.seekp((leitura.id-1)*sizeof(StructFuns));
    arquivo.read((char*)(&leitura),sizeof(StructFuns));

    do
    {
        system("cls");
        cout<<"O que voce deseja alterar nos seus dados?"<<endl;
        cout<<"[1] - Nome completo\n[2] - Sexo\n[3] - Telefone para contato\n[4] - Endere�o onde mora\n[5] - Para voltar ao menu anterior."<<endl;
        cin>>op;
        switch(op)
        {
        case 1:
            cout<<"Informe o novo que voce deseja salvar"<<endl;
            fflush(stdin);
            cin.getline(leitura.nome,50);
            break;
        case 2:
            cout<<"Informe o sexo que voce se identifica"<<endl;
            fflush(stdin);
            cin.getline(leitura.sexo,15);
            break;
        case 3:
            cout<<"Informe o novo telefone para contato"<<endl;
            fflush(stdin);
            cin.getline(leitura.telefone,15);
            break;
        case 4:
            cout<<"Informe o novo endereco que voce se encontra!"<<endl;
            fflush(stdin);
            cin.getline(leitura.local,50);
            break;

        }

    }
    while(op!=5);
    arquivo.close();
}

}

void MenuInicial(int verificadordelogin)
{
    setlocale(LC_ALL,"portuguese");
    int Menu, i, inteiro;
    char senha[10] = "";
    char senhaa[12] = "";
    char senhapadrao[12] = "senhapadrao";
    char senhapadraop[10] = "professor";
    if (verificadordelogin<0)
    {
        cout << "Tentativa de logins excedidas, por favor inicie o programa novamente."<<endl;
        exit(1);
    }
    do
    {
        system("color 9f"); /* Muda a cor do fundo */
        textcolor(LIGHTBLUE);
        textbackground(WHITE);

        gotoxy(15, 5);
        cout <<"                                                                                            ";
        gotoxy(15, 6);
        cout <<"                                 BEM VINDO AO MENU PRINCIPAL                                ";
        gotoxy(15, 7);
        cout <<"                                                                                            ";

        cout <<endl;
        gotoxy(15, 11);
        cout << "                            ";
        gotoxy(15, 12);
        cout << "  1- Menu do Coordenador  ";
        gotoxy(15, 13);
        cout << "                            ";

        gotoxy(15, 15);
        cout << "                            ";
        gotoxy(15, 16);
        cout << "    2- Menu do Professor    ";
        gotoxy(15, 17);
        cout << "                            ";

        gotoxy(15, 19);
        cout << "                            ";
        gotoxy(15, 20);
        cout << "      3- Menu do Aluno      ";
        gotoxy(15, 21);
        cout << "                            ";

        gotoxy(79, 11);
        cout << "                            ";
        gotoxy(79, 12);
        cout << "   4- Menu do Funcionario   ";
        gotoxy(79, 13);
        cout << "                            ";

        gotoxy(79, 15);
        cout << "                            ";
        gotoxy(79, 16);
        cout << " 5- Info. sobre o programa  ";
        gotoxy(79, 17);
        cout << "                            ";

        gotoxy(79, 19);
        cout << "                            ";
        gotoxy(79, 20);
        cout << "   6- Encerrar o programa   ";
        gotoxy(79, 21);
        cout << "                            ";

        textcolor(WHITE);
        textbackground(LIGHTBLUE);
        gotoxy(53,25);
        cout << "Digite a op��o: ";
        cin>>Menu;

        switch(Menu)
        {
        case 1:
            system("CLS");
            system("color 9f"); /* Muda a cor do fundo */
            textcolor(LIGHTBLUE);
            textbackground(WHITE);

            gotoxy(15, 5);
            cout <<"                                                                                            ";
            gotoxy(15, 6);
            cout <<"                              ENTRE COM A SENHA DO COORDENADOR                              ";
            gotoxy(15, 7);
            cout <<"                                                                                            ";
            gotoxy(15, 9);
            textcolor(WHITE);
            textbackground(LIGHTBLUE);
            cout << "Senha: ";
            for(int i = 0; i < 11; i++)
            {
                *(senhaa + i) = getch();
                printf("*");
            }

            inteiro = strcmp(senhaa, senhapadrao);
            if(inteiro == 0)
            {
                getch();
                namespaceGraphic::verificarSenha();
                MenuCoordenacao();
            }
            else
            {
                namespaceGraphic::verificarSenha();
                gotoxy(15,15);
                cout << "Senha Errada! Tente Novamente";
                getch();
                system("cls");
                MenuInicial(verificadordelogin -1 );
            }
            break;
        case 2:
            system("CLS");
            textcolor(LIGHTBLUE);
            textbackground(WHITE);
            gotoxy(15, 5);
            cout <<"                                                                                            ";
            gotoxy(15, 6);
            cout <<"                                INFORME A SENHA DO PROFESSOR                                ";
            gotoxy(15, 7);
            cout <<"                                                                                            ";
            gotoxy(15, 9);
            textcolor(WHITE);
            textbackground(LIGHTBLUE);
            cout << "Senha:";

            for(int i = 0; i < 9; i++)
            {
                *(senha + i) = getch();
                printf("*");
            }

            inteiro = strcmp(senha, senhapadraop);

            if(inteiro == 0)
            {
                getch();
                namespaceGraphic::verificarSenha();
                MenuProfessor();
            }
            else
            {
                namespaceGraphic::verificarSenha();
                gotoxy(15,15);
                cout << "Senha Errada! Tente Novamente";
                getch();
                system("cls");
                MenuInicial(verificadordelogin -1 );
            }
            break;
        case 3:
            system("cls");
            system("color 9f"); /* Muda a cor do fundo */
            textcolor(LIGHTBLUE);
            textbackground(WHITE);

            gotoxy(15, 5);
            cout <<"                                                                                            ";
            gotoxy(15, 6);
            cout <<"                                 BEM VINDO AO MENU DO ALUNO                                 ";
            gotoxy(15, 7);
            cout <<"                                                                                            ";
            ListarAlunoUnico();
            break;
        case 4:
            system("cls");
            system("color 9f"); /* Muda a cor do fundo */
            textcolor(LIGHTBLUE);
            textbackground(WHITE);

            gotoxy(15, 5);
            cout <<"                                                                                            ";
            gotoxy(15, 6);
            cout <<"                              BEM VINDO AO MENU DO FUNCIONARIO                              ";
            gotoxy(15, 7);
            cout <<"                                                                                            ";
            FunSpace::MenuFunMenu(arquivofun);
            system("cls");
            break;
        case 5:
            system("cls");
            string s;
            ifstream ler("Tutorial.txt");
            while(getline(ler,s))
            {
                cout<<s<<endl;
            }
            system("pause");
            system("CLS");
            break;

        }
    }
    while(Menu!=6);
}

void MenuCoordenacao()
{
    int opnovo;
    system("CLS");
    do
    {
        system("color 9f");
        textcolor(LIGHTBLUE);
        textbackground(WHITE);

        gotoxy(15, 5);
        cout <<"                                                                                            ";
        gotoxy(15, 6);
        cout <<"                             BEM VINDO AO MENU DO COORDENADOR                             ";
        gotoxy(15, 7);
        cout <<"                                                                                            ";
        gotoxy(15, 9);
        cout <<"                          Informe a opcao que deseja acessar                        \n";

        gotoxy(15, 14);
        cout << "                            ";
        gotoxy(15, 15);
        cout << "    1- Dados dos Alunos     ";
        gotoxy(15, 16);
        cout << "                            ";

        gotoxy(15, 18);
        cout << "                            ";
        gotoxy(15, 19);
        cout << "  2- Dados de professores   ";
        gotoxy(15, 20);
        cout << "                            ";

        gotoxy(79, 14);
        cout << "                            ";
        gotoxy(79, 15);
        cout << " 3- Dados dos funcionarios  ";
        gotoxy(79, 16);
        cout << "                            ";

        gotoxy(79, 18);
        cout << "                            ";
        gotoxy(79, 19);
        cout << "4- Voltar ao menu principal ";
        gotoxy(79, 20);
        cout << "                            ";

        gotoxy(54,25);
        cout << "Digite a opcao: ";
        cin>>opnovo;

        system("cls");
        switch(opnovo)
        {
        case 1:
            DadosAlunos();
            system("cls");
            break;
        case 2:
            DadosProf();
            system("cls");
            break;
        case 3:
            DadosFun();
            system("cls");
            break;
        case 4:
            system("cls");
            break;
        }
    }
    while(opnovo!=4);
}

void DadosAlunos()
{
    int menucord;
    char op='s';
    do
    {
        system("color 9f");
        textcolor(LIGHTBLUE);
        textbackground(WHITE);

        gotoxy(15, 5);
        cout <<"                                                                                            ";
        gotoxy(15, 6);
        cout <<"                                     O QUE DESEJA FAZER                                     ";
        gotoxy(15, 7);
        cout <<"                                                                                            ";

        gotoxy(15, 14);
        cout << "                            ";
        gotoxy(15, 15);
        cout << " 1- Criar arquivo do aluno  ";
        gotoxy(15, 16);
        cout << "                            ";

        gotoxy(15, 18);
        cout << "                            ";
        gotoxy(15, 19);
        cout << "      2- Listar Alunos      ";
        gotoxy(15, 20);
        cout << "                            ";

        gotoxy(15, 22);
        cout << "                            ";
        gotoxy(15, 23);
        cout << "     3- Cadastrar aluno     ";
        gotoxy(15, 24);
        cout << "                            ";

        gotoxy(79, 14);
        cout << "                            ";
        gotoxy(79, 15);
        cout << "4- Alterar os dados do aluno";
        gotoxy(79, 16);
        cout << "                            ";

        gotoxy(79, 18);
        cout << "                            ";
        gotoxy(79, 19);
        cout << "      5- Remover aluno      ";
        gotoxy(79, 20);
        cout << "                            ";

        gotoxy(79, 22);
        cout << "                            ";
        gotoxy(79, 23);
        cout << "6- Volt. ao menu coordenacao";
        gotoxy(79, 24);
        cout << "                            ";

        gotoxy(54,25);
        cout << "Digite a opcao: ";
        cin >> menucord;

        system("cls");
        switch(menucord)
        {
        case 1:
            IniciarArquivoAlunos();
            system("cls");
            break;
        case 2:
            ListarAlunos();
            system("cls");
            break;
        case 3:
            do
            {
                CadastraAlunos();
                cout<<"Deseja cadastrar outro aluno [s/n]?"<<endl;
                cin>>op;
            }
            while(op=='s'||op=='S');
            system("cls");
            break;
        case 4:
            AtualizarAluno();
            system("cls");
            break;
        case 5:
            RemoverAluno();
            system("cls");
            break;
        }
    }
    while (menucord!=6);

}

void IniciarArquivoAlunos()
{
    char opchar;

    ofstream arquivo(ArquivoAlunos,ios::trunc);

    if (arquivo.is_open())
    {
        StructAlunos Vazio= {0, " "," ", " "," "," "," ", 0.0, 0.0, 0.0, 0.0, 0};
        cout<<"Voc� deseja criar ou formatar o arquivo dos alunos do zero [s/n]?"<<endl
            << "S para sim e N para nao"<<endl;
        cin>>opchar;
        if(opchar=='s' || opchar == 'S')
        {
            opchar= ' ';
            cout<<"Tem certeza? O arquivo dos alunos ira ser criado ou formatado [s/n]."<<endl;
            cout << "Digite a op��o: ";
            cin>>opchar;
            if(opchar=='s'||opchar=='S')
            {
                for(int i=0; i<qtdaluno; i++)
                {
                    arquivo.write((const char*)(&Vazio),sizeof(StructAlunos));
                }
            }
            cout<<"Arquivo alunos criado/formatado com sucesso"<<endl;
        }

    }
    else
    {
        cout<<"O arquivo nao existe!!!"<<endl;
        cout<<"Crie o arquivo da proxima vez, pressione qualquer tecla para continuar"<<endl;
        system("cls");
        exit(1);
    }

    arquivo.close();

}

void CadastraAlunos()
{
    fstream entrada;
    int aux;
    entrada.open(ArquivoAlunos,ios::out|ios::in|ios::ate);
    if(entrada.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        exit(1);
    }
    StructAlunos Inserir;
    do
    {
        system("cls");
        cout<<"Digite a matr�cula do aluno"<<endl;
        cin>> aux;

    }
    while(aux <= 0 || aux > 500);

    entrada.seekg((aux-1) * sizeof(StructAlunos));

    entrada.read((char*)(&Inserir),sizeof(StructAlunos));

    if(Inserir.Matricula == 0)
    {
        Inserir.Matricula = aux;
        cout<<"Digite o nome do aluno"<<endl;
        fflush(stdin);
        cin.getline(Inserir.Nome,50);
        cout<<"Digite o nome do pai do aluno"<<endl;
        fflush(stdin);
        cin.getline(Inserir.NomePai,50);
        cout<<"Digite o nome da m�e do aluno"<<endl;
        fflush(stdin);
        cin.getline(Inserir.NomeMae,50);
        cout<<"Digite o sexo do aluno(m ou f)"<<endl;
        cin>>Inserir.Sexo;
        cout<<"Informe o telefone para contato do aluno"<<endl;
        cin>>Inserir.Telefone;
        cout<<"Informe o endere�o do aluno"<<endl;
        fflush(stdin);
        cin.getline(Inserir.Endereco,50);

        entrada.seekp((aux-1)*sizeof(StructAlunos));

        entrada.write((const char*)(&Inserir),sizeof(StructAlunos));
    }
    else
    {
        cout << " Aluno Matriculado " << endl;
    }

    entrada.close();
}

void ListarAlunos()
{
    ifstream arquivo(ArquivoAlunos);
    StructAlunos listar;

    int k=0;
    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo! "<<endl;
        exit(0);
    }
    system("CLS");

    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    gotoxy(15, 5);
    cout <<"                                                                                            ";
    gotoxy(15, 6);
    cout <<"                                    LISTA DE ALUNOS                                  ";
    gotoxy(15, 7);
    cout <<"                                                                                            ";
    textcolor(WHITE);
    textbackground(WHITE+29);
    gotoxy(15, 8);
    cout << setiosflags(ios::left)
         << setw(10) << "Matricula"
         << setw(15) << "Nome do Aluno"
         << setw(15) << "Nome do Pai"
         << setw(15) << "Nome da M�e"
         << setw(10) << "Sexo"
         << setw(20) << "Telefone"
         <<setw(20) << "Endere�o"<<endl;
    for(int contador = 0; contador <=qtdaluno; contador++)
    {
        arquivo.seekg((contador)*sizeof(StructAlunos));
        arquivo.read((char*)(&listar),sizeof(StructAlunos));
        if((listar.Matricula> 0) && (strcmp(listar.Nome,"")!=0))
        {
            gotoxy(15, 9+k);
            textcolor(WHITE);
            textbackground(WHITE+ 18);
            cout << setiosflags(ios::left)
                 << setw(10) << listar.Matricula
                 << setw(15) << listar.Nome
                 << setw(15) << listar.NomePai
                 << setw(15) << listar.NomeMae
                 << setw(10) << listar.Sexo
                 << setw(20) << listar.Telefone
                 <<setw(20)<<listar.Endereco<<endl;
            k++;
        }
    }

    gotoxy(15, 14);
    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    system("pause");
    system("cls");
    arquivo.close();
}

void AtualizarAluno()
{
    StructAlunos inserir;
    int Menuatualizar,mat;
    fstream  arquivo;

    arquivo.open(ArquivoAlunos,ios::out|ios::in|ios::ate);
    cout<<"Informe a matr�cula do Aluno que voc� deseja atualizar dados."<<endl;
    cout << "Digite a op��o: ";
    cin>>mat;

    arquivo.seekg((mat-1)*sizeof(StructAlunos));
    arquivo.read((char*)(&inserir),sizeof(StructAlunos));

    if(inserir.Matricula != 0)
    {
        cout<< "O que deseja atualizar?\n[1]-Nome do aluno\n[2]-Nome do Pai\n[3]-Nome da M�e\n[4]-Sexo\n[5]-Telefone\n[6]-Endere�o\n[7]-Voltar"<<endl;
        cout << "Digite a op��o: ";
        cin>>Menuatualizar;
        system("CLS");

        switch(Menuatualizar)
        {
        case 1:
            cout<<"Informe o  nome do aluno: "<<endl;
            fflush(stdin);
            cin.getline(inserir.Nome,50);
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;
        case 2:
            cout<<"Informe o novo Nome do Pai: "<<endl;
            fflush(stdin);
            cin.getline(inserir.NomePai,50);
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;
        case 3:
            cout<<"Informe o novo Nome da M�e: "<<endl;
            fflush(stdin);
            cin.getline(inserir.NomeMae,50);
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;
        case 4:
            cout<<"Informe o novo Sexo do Aluno: "<<endl;
            cin>>inserir.Sexo;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;
        case 5:
            cout<<"Informe o novo Telefone para Contato: "<<endl;
            cin>>inserir.Telefone;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;
        case 6:
            cout<<"Informe o novo endere�o do aluno"<<endl;
            fflush(stdin);
            cin.getline(inserir.Endereco,50);
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
            system("CLS");
            break;

        }
    }
    getchar();
    arquivo.close();
}

void RemoverAluno()
{
    StructAlunos inserir;
    StructAlunos Vazio = {0," "," "," "," "," "," ",0.0,0.0,0.0,0.0};

    fstream arquivo;

    arquivo.open(ArquivoAlunos,ios::out|ios::in|ios::ate);

    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        exit(0);
    }

    cout<<"Informe a matr�cula do aluno que voc� deseja remover: "<<endl;
    cin>>inserir.Matricula;
    arquivo.seekp((inserir.Matricula-1)*sizeof(StructAlunos));

    arquivo.write((const char*)(&Vazio),sizeof(StructAlunos));

    arquivo.close();
}

void DadosFun()
{
    int opcao;
    do
    {
        system("color 9f");
        textcolor(LIGHTBLUE);
        textbackground(WHITE);
        gotoxy(15, 9);
        cout <<"                                     O QUE DESEJA FAZER                                     ";

        gotoxy(15, 12);
        cout << "                            ";
        gotoxy(15, 13);
        cout << "  1-Criar Arquivo Inicial   ";
        gotoxy(15, 14);
        cout << "                            ";

        gotoxy(15, 16);
        cout << "                            ";
        gotoxy(15, 17);
        cout << "   2-Inserir Funcionario    ";
        gotoxy(15, 18);
        cout << "                            ";

        gotoxy(15, 20);
        cout << "                            ";
        gotoxy(15, 21);
        cout << "   3-Listar Funcionarios    ";
        gotoxy(15, 22);
        cout << "                            ";

        gotoxy(79, 12);
        cout << "                            ";
        gotoxy(79, 13);
        cout << "4-Atua. dados do funcionario";
        gotoxy(79, 14);
        cout << "                            ";

        gotoxy(79, 16);
        cout << "                            ";
        gotoxy(79, 17);
        cout << "   5-Remover Funcionario    ";
        gotoxy(79, 18);
        cout << "                            ";

        gotoxy(79, 20);
        cout << "                            ";
        gotoxy(79, 21);
        cout << "           0-Sair           ";
        gotoxy(79, 22);
        cout << "                            ";


        textcolor(WHITE);
        textbackground(LIGHTBLUE);
        gotoxy(54, 25);
        cout << "Escolha uma opcao: ";
        cin >> opcao;

        if(opcao < 0 || opcao > 5)
        {
            gotoxy(15, 26);
            cout<<"Opcao invalida! Informe uma opcao valida, por favor: ";
            getch();
            system("cls");

        }
        switch (opcao)
        {
        case 1:
            FunSpace::CriarArquivoFun(arquivofun);
            system("cls");
            break;
        case 2:
            FunSpace::CadastrarFun(arquivofun);
            system("cls");
            break;
        case 3:
            FunSpace::ListarFuncionarios(arquivofun);
            break;
        case 4:
            FunSpace::AlterarDadosFun(arquivofun);
            system("cls");
            break;
        case 5:
            FunSpace::RemoveFuncionario(arquivofun);
            system("cls");
            break;
        }

    }
    while (opcao != 0);
    MenuCoordenacao();
}

void DadosProf()
{
    int  op,aux;
    do
    {
        cout<<"O que voc� deseja acessar nos dados dos professores?"<<endl;
        cout<<"1 - Para criar ou recriar o arquivo dos  professores\n2 - Para cadastar um professor ao sistema\n3 - Para listar todos os professores cadastrados\n4 - Para remover um professor cadastrado\n5 - Para voltar ao menu anterior"<<endl;
        cin>>op;
        if(op<1 || op>5)
        {
            do
            {
                cout<<"Op��o inv�lida! Informe uma op��o v�lida, por favor."<<endl;
                cin>>aux;
            }
            while(aux<1 || aux>5);
            op=aux;
        }

        switch(op)
        {
        case 1:
            IniciarArquivoProf();
            system("cls");
            break;
        case 2:
            CadastraProf();
            system("cls");
            break;
        case 3:
            ListarProf();
            system("cls");
            break;
        case 4:
            RemoverProfessor();
            system("cls");
            break;
        }
    }
    while(op!=5);
}

void IniciarArquivoProf()
{
    char opchar;

    ofstream arquivo(ArquivoProfessores,ios::trunc);

    if (arquivo.is_open())
    {
        cout <<"Bem vindo a fun��o de criar ou recriar arquivo de professores!"<<endl;
        StructProfessor Vazio= {0," "," "," ", " ", 0.0};
        cout<<"Voc� deseja criar o arquivo dos professores do zero?"<<endl
            << "S para sim e N para nao"<<endl;
        cin>>opchar;
        if(opchar=='s' || opchar == 'S')
        {
            opchar= ' ';
            cout<<"Tem certeza? O arquivo professores ir� ser criado ou formatado."<<endl;
            cin>>opchar;
            if(opchar=='s'||opchar=='S')
            {
                for(int i=0; i<qtdprof; i++)
                {
                    arquivo.write((const char*)(&Vazio),sizeof(StructProfessor));
                }
            }
            cout<<"Arquivo professores criado com sucesso"<<endl;
            cout<<"Pressione qualquer tecla para continuar"<<endl;
            getch();
        }
    }
}

void CadastraProf()
{
    StructProfessor cadProf;
    int prof;
    fstream arquivo;
    arquivo.open(ArquivoProfessores,ios::out|ios::in|ios::ate);
    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        getch();
        exit(1);
    }
    do
    {
        cout<<"Id do professor"<<endl;
        cin>>prof;
    }
    while(prof <= 0 || prof > 200);

    arquivo.seekg((prof-1) * sizeof(StructProfessor));

    arquivo.read((char*)(&cadProf),sizeof(StructProfessor));

    if(cadProf.id == 0)
    {
        cout<<"Informe o nome"<<endl;
        fflush(stdin);
        cin.getline(cadProf.nome,50);
        cout<<"Informe o sexo"<<endl;
        fflush(stdin);
        cin.getline(cadProf.sexo,25);
        cout<<"Informe o endereco do professor"<<endl;
        fflush(stdin);
        cin.getline(cadProf.endereco,50);
        cout<<"Informe o telefone"<<endl;
        fflush(stdin);
        cin.getline(cadProf.telefone,15);
        do
        {
            cout<<"Informe o salario"<<endl;
            cin>>cadProf.salario;
        }
        while(cadProf.salario <= 0);
        arquivo.seekp((prof - 1)*sizeof(StructProfessor));
        arquivo.write((const char*)(&cadProf),sizeof(StructProfessor));
    }
    else
    {
        cout << " Professor cadastrado " << endl;
    }
    arquivo.close();
}

void ListarProf()
{
    StructProfessor prof;
    ifstream arquivo(ArquivoProfessores);
    int k=0;

    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo! "<<endl;
        exit(0);
    }
    system("CLS");

    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    gotoxy(15, 5);
    cout <<"                                                                                            ";
    gotoxy(15, 6);
    cout <<"                                    LISTA DE PROFESSORES                                   ";
    gotoxy(15, 7);
    cout <<"                                                                                            ";
    textcolor(WHITE);
    textbackground(WHITE+29);
    gotoxy(15, 8);
    cout << setiosflags(ios::left)
         << setw(5) << "ID"
         << setw(15) << "Nome"
         << setw(8) << "Sexo"
         << setw(10) << "Endereco"
          << setw(15) << "Telefone"
         << resetiosflags(ios::left)
         << setw(10) << "Salario" << endl;
    for(int contador = 0; contador <=qtdprof; contador++)
    {
        arquivo.seekg((contador)*sizeof(StructProfessor));
        arquivo.read((char*)(&prof),sizeof(StructProfessor));
        if(prof.id != 0)
        {
            gotoxy(15,9+k);
            textcolor(WHITE);
            textbackground(WHITE + 18);
            cout << setiosflags(ios::left)
                 << setw(5) << prof.id
                 << setw(15) << prof.nome
                 << setw(8) << prof.sexo
                  << setw(10) << prof.endereco
                   << setw(15) << prof.telefone
                 << setw(10) << setprecision(2) << resetiosflags(ios::left)
                 << setiosflags(ios::fixed | ios::showpoint) << prof.salario << '\n';
            k++;
        }
    }

    gotoxy(15, 14);
    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    system("pause");
    system("cls");
    arquivo.close();
}

void MenuProfessor()
{
    int op;
    system("cls");
    do
    {
        system("cls");
        cout<<"O que voc� deseja, professor?"<<endl;
        cout<<"[1] - Para colocar faltas nos alunos\n[2] - Para inserir as notas dos alunos\n[3] - Para voltar ao menu principal"<<endl;
        cout<<"Insira a op��o desejada: ";
        cin>>op;
        switch(op)
        {
        case 1:
            InsereFaltas();
            system("cls");
            break;
        case 2:
            AlterarNotasAluno();
            break;
        }
    }
    while(op!=3);
    system("cls");
}

void InsereFaltas()
{
    int Matricula;
    StructAlunos inserir;
    fstream  arquivo;
    arquivo.open(ArquivoAlunos,ios::out|ios::in|ios::ate);
    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        getch();
    }

    cout<<"informe a matr�cula do Aluno que voc� deseja atualizar as faltas"<<endl;
    cin>>Matricula;

    arquivo.seekg((Matricula-1)*sizeof(StructAlunos));
    arquivo.read((char*)(&inserir),sizeof(StructAlunos));

    if(inserir.Matricula != 0)
    {
        cout<<"Informe quantas faltas o aluno possui : "<<endl;
        cin>>inserir.faltas;
        arquivo.seekp((Matricula-1)*sizeof(StructAlunos));
        arquivo.write((const char*)(&inserir),sizeof(StructAlunos));
        arquivo.close();
        cout<<"As faltas foram inseridas no aluno informado"<<endl;
    }
    else
    {
        cout << " Aluno nao cadastrado " << endl;
    }
    getchar();
}

void FuncaoAlunoMenu()
{
    int faltascomp;
    double mediaaluno = 0.0;
    double mediafinal = 0.0;

    fstream arquivo;

    arquivo.open(ArquivoAlunos,ios::in);

    StructAlunos listar;

    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo! "<<endl;
        getch();
    }

    cout << setiosflags(ios::left)
         << setw(15) << "Matricula"
         << setw(15) << "Nome"
         << setw(15) << "Nome do Pai"
         << setw(15) << "Nome da M�e"
         << setw(10) << "Sexo"
         << setw(10) << "Telefone"
         << resetiosflags(ios::left)
         << setw(10) << "1� Nota"
         << setw(10) << "2� Nota"
         << setw(10) << "3� Nota"
         << setw(10) << "4� Nota"
         << setw(10) <<  "Faltas"
         << setw(10) <<  "M�dia"
         << setw(15) <<  "Situa��o" <<endl;

    arquivo.read((char*)(&listar),sizeof(StructAlunos));
    pontmedia med;
    med = new double[4];

    while(arquivo && !arquivo.eof()&& listar.Matricula!=0)
    {
        mediaaluno = 0.0;
        mediafinal = 0.0;

        med[1]=listar.Nota1;
        med[2]=listar.Nota2;
        med[3]=listar.Nota3;
        med[4]=listar.Nota4;
        for(int x=1; x<=4; x++)
        {
            mediaaluno = mediaaluno + med[x];
        }
        mediafinal = mediaaluno/4;

        cout << setiosflags(ios::left)
             << setw(15) << listar.Matricula
             << setw(15) << listar.Nome
             << setw(15) << listar.NomePai
             << setw(15) << listar.NomeMae
             << setw(10) << listar.Sexo
             << setw(10) << listar.Telefone
             << resetiosflags(ios::left)<<setprecision(2)<<setiosflags(ios::fixed|ios::showpoint)
             << setw(10) << listar.Nota1
             << setw(10) << listar.Nota2
             << setw(10) << listar.Nota3
             << setw(10) << listar.Nota4
             << setw(10) << listar.faltas
             << setw(10) << mediafinal ;
        faltascomp = listar.faltas;
        if(mediafinal>=6.0)
        {
            if(faltascomp<=25)
            {
                cout << setw(15) << " Aprovado!" << endl;
            }
            else
            {
                cout << setw(15) << " Rep por Faltas!" << endl;
            };
        }
        if(mediafinal<6.0)
        {
            if(faltascomp<=25)
            {
                cout << setw(15) << " Reprovado por M�dia!" << endl;
            }
            else
            {
                cout << setw(15) << " Reprovado por M�dia e Faltas!" << endl;
            }
            if(mediafinal>=6.0)
            {
                if(faltascomp<=25)
                {
                    cout << setiosflags(ios::right)
                         << setw(15) << " Aprovado!" << endl;
                }
                else
                {
                    cout << setiosflags(ios::right)
                         << setw(15) << " Reprovado por Faltas!" << endl;
                };
            }


        }

        arquivo.read((char*)(&listar),sizeof(StructAlunos));


    }
    delete [] med;
    med = NULL;

    arquivo.close();
}

void AlterarNotasAluno()
{

    StructAlunos InserirNotasAlunos;
    int mat;
    int Menuatualizar;
    fstream  arquivo;

    arquivo.open(ArquivoAlunos,ios::out|ios::in|ios::ate);

    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        getch();
    }

    cout<<"Informe a matricula do aluno que voc� deseja atualizar as notas:"<<endl;
    cin>>mat;

    arquivo.seekg((mat-1)*sizeof(StructAlunos));
    arquivo.read((char*)(&InserirNotasAlunos),sizeof(StructAlunos));

    if(InserirNotasAlunos.Matricula != 0)
    {
      do
    {
        cout<< "Que nota voc� deseja alterar? [1]- 1� Nota\n[2]- 2� Nota\n[3]- 3� Nota\n[4]- 4� Nota\n[5]- Sair"<<endl;
        cin>>Menuatualizar;
        switch(Menuatualizar)
        {
        case 1:
            cout<<"Informe a nova 1� Nota: "<<endl;
            cin>>InserirNotasAlunos.Nota1;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&InserirNotasAlunos),sizeof(StructAlunos));
            break;
        case 2:
            cout<<"Informe a nova 2� Nota: "<<endl;
            cin>>InserirNotasAlunos.Nota2;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&InserirNotasAlunos),sizeof(StructAlunos));
            break;
        case 3:
            cout<<"Informe a nova 3� Nota: "<<endl;
            cin>>InserirNotasAlunos.Nota3;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&InserirNotasAlunos),sizeof(StructAlunos));
            break;
        case 4:
            cout<<"Informe a nova 4� Nota: "<<endl;
            cin>>InserirNotasAlunos.Nota4;
            arquivo.seekp((mat-1)*sizeof(StructAlunos));
            arquivo.write((const char*)(&InserirNotasAlunos),sizeof(StructAlunos));
            break;
        }
    }
    while(Menuatualizar!=5);
    arquivo.close();
    }
}

void ListarAlunoUnico()
{
    double mediaaluno = 0.0;
    double mediafinal = 0.0;
    int faltascomp,Matricula;

    fstream arquivo;
    StructAlunos listar;
    arquivo.open(ArquivoAlunos,ios::in);

    gotoxy(15,12);
    textcolor(WHITE);
    textbackground(LIGHTBLUE);
    cout << "Informe sua matricula: ";
    cin >> Matricula;
    system("CLS");

    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo! "<<endl;
        getch();
    }

    cout << setiosflags(ios::left)
         << setw(10) << "Matricula"
         << setw(15) << "Nome"
         << setw(15) << "Nome do Pai"
         << setw(15) << "Nome da M�e"
         << setw(10) << "Sexo"
         << setw(20) << "Telefone"
         << setw(10) << "1�Nota"
         << setw(10) << "2�Nota"
         << setw(10) << "3�Nota"
         << setw(10) << "4�Nota"
         << setw(10) << "Faltas"
         << setw(10) <<  "M�dia"
         << setw(15) <<  "Situa��o" <<endl;

    arquivo.seekp((Matricula-1)*sizeof(StructAlunos));
    arquivo.read((char*)(&listar),sizeof(StructAlunos));
    if(listar.Matricula != 0)
    {
        pontmedia med;
        med = new double[4];

        med[1]=listar.Nota1;
        med[2]=listar.Nota2;
        med[3]=listar.Nota3;
        med[4]=listar.Nota4;
        for(int x=1; x<=4; x++)
        {
            mediaaluno = mediaaluno + med[x];
        }
        mediafinal = mediaaluno/4;

        cout << setiosflags(ios::left)
             << setw(10) << listar.Matricula
             << setw(15) << listar.Nome
             << setw(15) << listar.NomePai
             << setw(15) << listar.NomeMae
             << setw(10) << listar.Sexo
             << setw(20) << listar.Telefone
             << setw(10) << listar.Nota1
             << setw(10) << listar.Nota2
             << setw(10) << listar.Nota3
             << setw(10) << listar.Nota4
             << setw(10) << listar.faltas
             << setw(10) << mediafinal;
        faltascomp = listar.faltas ;

        delete [] med;
        med = NULL;

        if(mediafinal>=6.0)
        {
            if(faltascomp<=25)
            {

                cout << setw(15) << " Aprovado!" << endl;
            }
            else
            {
                cout << setw(15) << " Reprovado por Faltas!" << endl;
            };
        }
        if(mediafinal<6.0)
        {
            if(faltascomp<=25)
            {
                cout << setw(12) << " Reprovado por M�dia!" << endl;
            }
            else
            {
                cout << setw(15) << " Reprovado por M�dia e Faltas!" << endl;
            }
        }
    }
    else
    {
        cout << " Aluno nao encontrado " << endl;
    }
arquivo.close();
getch();
system("cls");
}

void RemoverProfessor()
{
   StructProfessor Inserir;
   StructProfessor FichaVazia = {0,"","","","",0.0};
    fstream arquivo;
    arquivo.open(ArquivoProfessores,ios::out|ios::in|ios::ate);
    if(arquivo.fail())
    {
        cout<<"Falha na abertura do arquivo!"<<endl;
        getch();
    }
    cout<<"Informe o ID do professor que deseja remover: "<<endl;
    cin>>Inserir.id;

    if(Inserir.id != 0)
    {
      arquivo.seekp((Inserir.id-1)*sizeof(StructProfessor));
      arquivo.write((const char*)(&FichaVazia),sizeof(StructProfessor));
      cout << " Professor removido " << endl;
      getchar();
    }
    else{
        cout << " Professor nao cadastrado " << endl;
        getchar();
    }
    arquivo.close();
}
